# Matomo role for Ansible

Install Matomo via Docker.

## How to Use

Define the following variables (i.e. in your `group_vars`):

- `certbot_contact_email`
- `nginx_server_name`
