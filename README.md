# ansible-jailbreak

Ansible playbooks for Jailbreak

For now it is testing stuff.

## Install requirements

```bash
ansible-galaxy install -r requirements.yml
```

## Install Debian Stretch server

First, create a new server on [Scaleway](https://console.scaleway.com/compute), choose Debian Stretch, give it a name and tags, and ensure one of its tags is the same value as the name.

```bash
export SCW_TOKEN="..." # secret from Jailbrak KeePassX passwords file
ansible-playbook --inventory scaleway_inventory.yml --limit server_name --user root playbooks/debian-stretch.yml
```

## Install Scaleway DEV1-S server

```bash
export SCW_TOKEN="..." # secret from Jailbrak KeePassX passwords file
ansible-playbook --inventory scaleway_inventory.yml --limit server_name --user root playbooks/scaleway-server-dev1-s.yml
```

## Install Matomo

```bash
export SCW_TOKEN="..." # secret from Jailbrak KeePassX passwords file
export MYSQL_ROOT_PASSWORD="..." # secret from Jailbrak KeePassX passwords file
ansible-playbook --inventory scaleway_inventory.yml --limit mink --user root playbooks/matomo.yml
```